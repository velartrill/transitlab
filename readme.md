transitlab
----------

by [ʞ] @velartrill

please note that transitlab is a language, this is just the reference implementation. it's
written in ruby b/c treetop is the only parser generator i can wrap my brain around, not
because ruby is good. ruby is terrible. don't use ruby.

requirements
------------
 * treetop (`gem install treetop`)
 * ruby

building
--------
 * `tt tparse.tt`
 * `ruby transit.rb <map file>`

map file syntax
---------------
yeah no
