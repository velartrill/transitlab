SCALE = 15
$minx = 0
$miny = 0
$maxx = 0
$maxy = 0

$places = []
$roads = []
$stations = []

def line_intercept(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y)
	
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if s >= 0 and s <= 1 and t >= 0 and t <= 1
        # Collision detected
        rx = p0_x + (t * s1_x);
        ry = p0_y + (t * s1_y);
        return rx, ry
    end

    throw "no collision detected!" # No collision
end

class Place
	def initialize(id,name,pos,size,kind)
		@id = id
		@name = name
		@pos = pos
		@size = size
		@kind = kind
	end
	
	def svg
		case @kind
			when :oval
			'<ellipse class="place" cx="'+@pos.x.to_s+'" cy="'+@pos.y.to_s+'" rx="'+@size.w.to_s+'" ry="'+@size.h.to_s+'"/>'
			when :rect
			'<rect class="place" x="'+(@pos.x-@size.w/2).to_s+'" y="'+(@pos.y-@size.h/2).to_s+'" width="'+@size.w.to_s+'" height="'+@size.h.to_s+'"/>'
		end
	end
end
class Size
	def initialize(w,h)
		@w = w
		@h = h
	end
	def w
		@w*SCALE
	end
	def h
		@h*SCALE
	end
end
class Location
	def x
		0
	end
	def y
		0
	end
end
class Autoname
	def name
		if @name==""
			id.upcase.sub "-", " "
		else
			@name
		end
	end
end
class Point < Location
	def initialize(x, y)
		@x = x
		@y = y
		$minx = @x if @x<$minx
		$miny = @y if @y<$miny
		$maxx = @x if @x>$maxx
		$maxy = @y if @y>$maxy
	end
	def x
		(@x - $minx) * SCALE
	end
	def y
		(@y - $miny) * SCALE
	end
end
class StationPos < Location
	def initialize(st)
		@st = st
	end
	def x
		st = nil
		$stations.each do |s|
			if s.id == @st
				st = s
				break
			end
		end
		throw "station not found!" if st == nil
		st.pos.x
	end
	def y
		st = nil
		$stations.each do |s|
			if s.id == @st
				st = s
				break
			end
		end
		throw "station not found!" if st == nil
		st.pos.y
	end
end
class Position < Location
	def initialize(road,pct)
		@road = road
		@pct = pct
	end
	def x
		rd = nil
		$roads.each do |r|
			if r.id == @road
				rd = r
				break
			end
		end
		throw "road not found!" if rd == nil
		
		rd.from.x + (rd.to.x - rd.from.x)*(@pct.to_f / 100.0)
	end
	def y
		rd = nil
		$roads.each do |r|
			if r.id == @road
				rd = r
				break
			end
		end
		throw "road not found!" if rd == nil
		
		rd.from.y + (rd.to.y - rd.from.y)*(@pct.to_f / 100.0)
	end
end
class Intersection < Location
	def initialize(id1, id2)
		@id1 = id1
		@id2 = id2
	end
	def xy
		r1 = nil
		r2 = nil
		$roads.each do |r|
			if r.id == @id1
				r1 = r
				break if r2 != nil
			elsif r.id == @id2
				r2 = r
				break if r1 != nil
			end
		end
		throw "could not find both roads in intersection" if r1==nil or r2==nil
		x,y = line_intercept(r1.from.x.to_f, r1.from.y.to_f, r1.to.x.to_f,
			r1.to.y.to_f, r2.from.x.to_f, r2.from.y.to_f, r2.to.x.to_f, r2.to.y.to_f)
		return [x,y]
	end
	def x
		xy[0]
	end
	def y
		xy[1]
	end
end

class Road < Autoname
	attr :id, :from, :to
	#kinds:
	#	road
	#	path
	#	oneway
	#	rail (grade?)
	def initialize(name, id, from, to, kind)
		@name = name
		@id = id
		@from = from
		@to = to
		@kind = kind
	end
	
	def svg
		cx = ((from.x + to.x) / 2).to_s
		cy = ((from.y + to.y) / 2).to_s
		fx = from.x.to_s; tx = to.x.to_s
		fy = from.y.to_s; ty = to.y.to_s
		'<line class="'+@kind.to_s+'" x1="'+fx+'" y1="'+fy+
		'" x2="'+tx+'" y2="'+ty+
		'"/>'
	end
	def text
		cx = ((from.x + to.x) / 2).to_s
		cy = (((from.y + to.y) / 2) + 5).to_s
		dx = to.x - from.x
		dy = to.y - from.y
		theta = Math.atan2(dy,dx)
		angle = theta * (180 / Math::PI)
		if angle < -90
			angle += 180
		elsif angle > 90
			angle -= 180
		end
		'<text class="'+@kind.to_s+'" x="'+cx+'" y="'+
		cy+'" transform="rotate('+angle.to_s+','+cx+','+cy+')">'+name+'</text>'
	end
end


class Station < Autoname
	attr :name, :id, :pos
	def initialize(name, id, pos)
		@name = name
		@id = id
		@pos = pos
	end
	def svg
		'<circle cx="'+@pos.x.to_s+'" cy="'+@pos.y.to_s+'" class="station" r="10"/>'
	end
end

require 'treetop'
require './tparse'

pr = TransitParser.new

if ARGV.size != 1
	puts "\x1b[1;31merror:\x1b[0m you sick fuck"
	exit 0
end

c = pr.parse(File.read(ARGV[0]))
if !c
	puts pr.failure_reason
end
raw = c.read


raw.each do |e|
	if e.is_a? Road
		$roads << e
	elsif e.is_a? Place
		$places << e
	elsif e.is_a? Station
		$stations << e
	end
end

$minx -= 4
$maxx += 4
$miny -= 4
$maxy += 4

svg = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1">'
svg << '<style type="text/css">
	text {
		font-family: "Open Sans", sans-serif;
		text-anchor: middle;
	}
	text.road {
		fill: #252525;
		font-size: 12pt;
		stroke: #DEDEDE;
		font-weight: bold;
		stroke-width: 1px;
	}
	line.road {
		stroke: #989898;
		stroke-linecap: round;
		stroke-width: 2;
	}
	line.path {
		stroke: #989898;
		stroke-width: 1;
		stroke-dasharray: 3 3;
	}
	text.path {
		fill: #252525;
		stroke: #DEDEDE;
		font-size: 10pt;
		stroke-width: .3pt;
	}
	text.rail {
		fill: #611A46;
		stroke: white;
		stroke-width: .3pt;
		font-size: 12pt;
	}
	line.rail {
		stroke: #814169;
		stroke-dasharray: 10 5 5 3 5;
		stroke-width: 2;
	}
	circle.station {
		fill: #57752E;
		stroke: #405720;
		stroke-width: 2;
	}
	rect.place, ellipse.place {
		fill: #2D8295;
		stroke: #205D6A;
		stroke-width: 2;
	}
</style>'
svg << '	<rect x="0" y="0" width="'+(($maxx-$minx)*SCALE).to_s+'" height="'+(($maxy-$miny)*SCALE).to_s+'" fill="#DEDEDE"/>'
$roads.each do |r|
	svg << "\n\t"
	svg << r.svg
end
$places.each do |p|
	svg << "\n\t"
	svg << p.svg
end
$stations.each do |s|
	svg << "\n\t"
	svg << s.svg
end


$roads.each do |r|
	svg << "\n\t"
	svg << r.text
end

svg << "\n</svg>"

print svg
